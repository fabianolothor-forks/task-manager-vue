# task-manager-vue

## How to Run this Project

```sh
# Clone

git clone https://gitlab.com/fabianolothor-forks/task-manager-vue.git
git clone https://gitlab.com/fabianolothor-forks/task-manager-laravel.git

# Setup DB

touch task-manager-laravel/storage/db.sqlite
chmod 777 task-manager-laravel/storage/db.sqlite

cp task-manager-laravel/.env.example task-manager-laravel/.env
echo DB_DATABASE=$PWD/task-manager-laravel/storage/db.sqlite >> ./task-manager-laravel/.env

# Prepare back-end

cd task-manager-laravel/
composer install
php artisan co:ca
php artisan migrate

# Run back-end

php artisan serve

### OPEN A NEW TERMINAL ###

# Prepare front-end

cd ../task-manager-vue/
npm install

# Run back-end

npm run serve
```